<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTresholdPerformanceNewSpvUploadResultAbsen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='treshold_performance_new_spv_upload_result_absen';
    public function up()
    {
        //
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->dateTime('start')->nullable();
                $table->dateTime('finish')->nullable();
                $table->bigInteger('worker')->nullable();
                $table->bigInteger('data')->nullable();
                $table->integer('status')->nullable();
                $table->dateTime('created_at')->nullable();
                $table->dateTime('updated_at')->nullable();
                $table->dateTime('deleted_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
