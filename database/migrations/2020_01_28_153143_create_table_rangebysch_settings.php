<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRangebyschSettings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='rangebysch_settings';
    public function up()
    {
        //
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');
                
                $table->string('schtype', 255)->nullable();	
                $table->string('shiftcode', 255)->nullable();
                $table->decimal('in_allowed_before', 5, 2)->nullable();
                $table->decimal('in_allowed_after', 5, 2)->nullable();
                $table->decimal('out_allowed_before', 5, 2)->nullable();
                $table->decimal('out_allowed_after', 5, 2)->nullable();
                $table->dateTime('created_at')->nullable();
                $table->dateTime('updated_at')->nullable();
                $table->dateTime('deleted_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
