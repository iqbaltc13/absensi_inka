<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHt45RfidTmp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='ht45_rfid_tmp';
    public function up()
    {
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->bigInteger('id_tmp')->nullable();
                $table->bigInteger('INDEKSFINGER')->nullable();
                $table->string('guid')->nullable();
                $table->string('rfid_epc')->nullable();
                $table->string('rfid_tid')->nullable();
                $table->string('rfid_userdata')->nullable();
                $table->string('rfid_arah')->nullable();
                $table->string('rfid_reader_ip')->nullable();
                $table->string('rfid_reader_mac')->nullable();
                $table->integer('rfid_reader_ant')->nullable();
                $table->dateTime('created_at')->nullable();
                $table->dateTime('updated_at')->nullable();
                $table->dateTime('deleted_at')->nullable();
                
                $table->string('nip')->nullable();
                $table->string('is_libur')->nullable();
                $table->string('is_ht45')->nullable();
                $table->integer('is_missing')->nullable();
                $table->string('flag_read_s1')->nullable();
               
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
