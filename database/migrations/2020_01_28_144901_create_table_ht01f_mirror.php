<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHt01fMirror extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='ht01f_mirror';

    public function up()
    {
        //
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->char('SCHTYPE', 10)->nullable();	
                $table->char('REGNO', 10)->nullable();	
                $table->integer('PERIODE')->nullable();	
                $table->integer('BULAN')->nullable();	
                $table->char('DAY1', 10)->nullable();	
                $table->char('DAY2', 10)->nullable();	
                $table->char('DAY3', 10)->nullable();	
                $table->char('DAY4', 10)->nullable();	
                $table->char('DAY5', 10)->nullable();	
                $table->char('DAY6', 10)->nullable();	
                $table->char('DAY7', 10)->nullable();	
                $table->char('DAY8', 10)->nullable();	
                $table->char('DAY9', 10)->nullable();	
                $table->char('DAY10', 10)->nullable();	
                $table->char('DAY11', 10)->nullable();	
                $table->char('DAY12', 10)->nullable();	
                $table->char('DAY13', 10)->nullable();	
                $table->char('DAY14', 10)->nullable();	
                $table->char('DAY15', 10)->nullable();	
                $table->char('DAY16', 10)->nullable();	
                $table->char('DAY17', 10)->nullable();	
                $table->char('DAY18', 10)->nullable();	
                $table->char('DAY19', 10)->nullable();	
                $table->char('DAY20', 10)->nullable();	
                $table->char('DAY21', 10)->nullable();	
                $table->char('DAY22', 10)->nullable();	
                $table->char('DAY23', 10)->nullable();	
                $table->char('DAY24', 10)->nullable();	
                $table->char('DAY25', 10)->nullable();	
                $table->char('DAY26', 10)->nullable();	
                $table->char('DAY27', 10)->nullable();	
                $table->char('DAY28', 10)->nullable();	
                $table->char('DAY29', 10)->nullable();	
                $table->char('DAY30', 10)->nullable();	
                $table->char('DAY31', 10)->nullable();	
                $table->char('CREATEBY', 20)->nullable();	
                $table->dateTime('CREATEDT')->nullable();	
                $table->char('MODIFYBY', 20)->nullable();	
                $table->dateTime('MODIFYDT')->nullable();
                $table->dateTime('created_at')->nullable();
                $table->dateTime('updated_at')->nullable();
                $table->dateTime('deleted_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
