<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableHt45Keshc extends Migration
{
    /**
     * id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='ht45_keshc';
    

    public function up()
    {
        //
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');

                $table->string('NOLOG')->nullable();
                $table->string('REGNO')->nullable();
                $table->string('TIPE')->nullable();
                $table->dateTime('TGLLOAD')->nullable();
                $table->integer('NOMESIN')->nullable();
                $table->string('IDFINGER')->nullable();
                $table->string('NAMANYA')->nullable();
                $table->integer('INDEKSFINGER')->nullable();
                $table->integer('MODEVERIFIKASI')->nullable();
                $table->integer('OTORITAS')->nullable();
                $table->integer('KODEINOUT')->nullable();
                $table->integer('THN')->nullable();
                $table->integer('BLN')->nullable();
                $table->integer('TGL')->nullable();
                $table->integer('JAM')->nullable();
                $table->integer('MENIT')->nullable();
                $table->integer('DETIK')->nullable();
                $table->string('LOGVERIF')->nullable();
                $table->dateTime('created_at')->nullable();
                $table->dateTime('updated_at')->nullable();
                $table->dateTime('deleted_at')->nullable();
                $table->integer('flag_upload_keshc')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
