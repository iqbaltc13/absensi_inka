<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableRangebyschHt04 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    private $tableName='rangebysch_ht04';
    public function up()
    {
        //
        if (!Schema::hasTable($this->tableName)) {
            Schema::create($this->tableName, function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('schtype', 255)->nullable();	
                $table->string('shiftcode', 255)->nullable();	
                $table->string('range_start', 255)->nullable();	
                $table->string('range_end', 255)->nullable();	
                $table->string('absen', 255)->nullable();	
                $table->string('kemaren', 255)->nullable();	
                
                $table->dateTime('created_at')->nullable();
                $table->dateTime('updated_at')->nullable();
                $table->dateTime('deleted_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
