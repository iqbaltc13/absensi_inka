<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HT08MirroringCron extends Model
{
    //
    protected $table = 'ht08_mirroring_cron';
    protected $guarded=[];
    
    public $timestamps = true;
    public function user()
	{
  
        return $this->hasOne('App\User', 'nip', 'REGNO')->withTrashed();
	}
}
