<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterTresholdDateNewSpv extends Model
{
    //
    protected $table = 'master_treshold_date_new_spv';
    public $timestamps = true;
    protected $guarded=[];
}
