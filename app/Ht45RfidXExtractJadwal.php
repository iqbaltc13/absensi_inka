<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ht45RfidXExtractJadwal extends Model
{
    //
    protected $table = 'extract_jadwal_pegawai_x_ht45_rfid_tmp';
    public $timestamps = true;
    protected $guarded=[];
    public function user(){
		
		return $this->hasOne('App\User', 'nip','nip')->withTrashed();
	
    }
    public function extractJadwal(){
		
		return $this->hasOne('App\ExtractJadwalPegawai', 'id','id_extract_jadwal');
	
    }
    public function dataTmp(){
		
		return $this->hasOne('App\RfidTmp ', 'id','id_ht45_rfid_tmp');
	
    }
}
