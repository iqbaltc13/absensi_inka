<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rfid extends Model
{
    //
    protected $table = 'ht45_rfid';
    protected $primaryKey = 'id';
    // protected $fillable = ['rfid_epc', 'rfid_tid', 'rfid_userdata', 'rfid_reader_ip', 'rfid_reader_mac', 
    //                          'rfid_reader_ant', 'created_at','nip'];
    protected $guarded=[];
    public function user()
	{
  
        return $this->hasOne('App\User', 'rfid_tid', 'rfid_tid')->withTrashed();
	}

    public function masterRfidReader(){
        return $this->belongsTo('App\MasterRfidReader', 'rfid_reader_mac', 'rfid_reader_mac');
    }
}
