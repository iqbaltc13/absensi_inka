<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TresholdPerformanceNewSpv extends Model
{
    //
    protected $table = 'treshold_performance_new_spv';
    public $timestamps = true;
    protected $guarded=[];
}
