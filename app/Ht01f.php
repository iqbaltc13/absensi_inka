<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ht01f extends Model
{
    //
    protected $table = 'ht01f_mirror';
    public $timestamps = true;
    protected $guarded=[];
}
