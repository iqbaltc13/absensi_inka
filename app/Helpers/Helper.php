<?php

namespace App\Helpers;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Client;
class Helper 
{
    public function __construct()
    {
        
        $this->client=new Client();
        $this->url= env('EOFFICE_PRD_URL', 'localhost').'api/v1/';
        $this->url_dev='https://eoffice-dev.inka.co.id/api/v1/';
        
    }
    public function injectHt45Ht08KeshcPrd($arrParse=[]){
        $formParams=[
            'tanggal'=>$arrParse['tanggal'],
            'nip'=>$arrParse['nip'],
            'kodeinout'=>$arrParse['kodeinout'],
            'created_at'=>$arrParse['date_time_detected'],
            'token'=>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjIwNDkzNDY2MjgsInN1YiI6bnVsbCwiaXNzIjoiaHR0cHM6Ly9lLW9mZmljZS5pbmthLmNvLmlkL2FwaS92MS9hdXRlbnRpa2FzaS9sb2dpbiIsImlhdCI6MTU3NTk2MTAyOCwibmJmIjoxNTc1OTYxMDI4LCJqdGkiOiJKd01ZdmRVRlBWYVR0YmZRIn0.b8MmaTp2mXhaKMp7J9A9IqBantTFVIqM-XMvP85DJ-M',
            'is_add_ht45'=>1,
            'is_add_clean'=>0,
        ];
        print_r($formParams);
        $url=$this->url.'absensi-insert-keshc';
        try{
            $getDataJson = $this->client->request('POST', $url, array('form_params' => $formParams));
        } catch (\Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

    public function injectHt45Ht08KeshcDev($arrParse=[]){
        $formParams=[
            'tanggal'=>$arrParse['tanggal'],
            'nip'=>$arrParse['nip'],
            'kodeinout'=>$arrParse['kodeinout'],
            'created_at'=>$arrParse['date_time_detected'],
            'token'=>'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjIwNTc4Mzk4NzgsInN1YiI6bnVsbCwiaXNzIjoiaHR0cHM6Ly9lb2ZmaWNlLWRldi5pbmthLmNvLmlkL2FwaS92MS9hdXRlbnRpa2FzaS9sb2dpbiIsImlhdCI6MTU4NDU0MDY3OCwibmJmIjoxNTg0NTQwNjc4LCJqdGkiOiJINlhvMDJoN0FHZTJRMDVZIn0.zOsYRKVlOC6Yva-bDy5MD4H1kxIxh_7VvBqJW94XIx1',
            'is_add_ht45'=>1,
            'is_add_clean'=>1,
        ];
        print_r($formParams);
        $url=$this->url_dev.'absensi-insert-keshc';
        try{
            $getDataJson = $this->client->request('POST', $url, array('form_params' => $formParams));
        } catch (\Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }
    public function cekIfAdaHt45Clean($nip){
        // Create a client with a base URI
        $url=env('EOFFICE_PRD_URL', 'localhost').'cek-absen-hari-ini/';
        $client = new GuzzleHttp\Client(['base_uri' => $url]);
        // Send a request to https://foo.com/api/test
        try{
            $response = $client->request('GET', $nip);
        } catch (\Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        $getData =  json_decode($response->getBody()->getContents(), true);
      
        
    }
    public function injectRfidNoTreshold($formParams){
        $formParams['token'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjIwMDc3NjI0NDUsInN1YiI6OTkxNTAwMDE3LCJpc3MiOiJodHRwOi8vMTk4LjcxLjgwLjE4OToxNDA0NS9hcGkvdjEvYXV0ZW50aWthc2kvbG9naW4iLCJpYXQiOjE1MzQzNzY4NDUsIm5iZiI6MTUzNDM3Njg0NSwianRpIjoid1dVUVBvVUFNMXFxU2xKVyJ9.xOKT-uqo1Shn67N7WCvrpEz4DWHRry-h7lXP3BCDHc4';
        $linkApi='http://103.20.90.17:8008/api/v1/inject-rfid-no-treshold';

        try{
            $data = $this->client->request('POST', $linkApi, array('form_params' => $formParams));
            $result =  json_decode($data->getBody()->getContents(), true);
            print_r($result);
        } catch (\Exception $e){
                            //echo $e->getResponse()->getBody();
                            //die();
             $this->errorIndexTimeStamp  = 2;
             echo $e->getMessage();

             
        }
    }
    public function getDataFinger($formParams){
        $linkApi='https://e-office.inka.co.id/api/v1/get-data-finger';

        try{
            $data = $this->client->request('POST', $linkApi, array('form_params' => $formParams));
            $result =  json_decode($data->getBody()->getContents(), true);
            print_r($result);
            return $result;
        } catch (\Exception $e){
                            //echo $e->getResponse()->getBody();
                            //die();
             $this->errorIndexTimeStamp  = 2;
             //echo $e->getMessage();
             $return = [];
             return $return;
        }
    }
}