<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TresholdPerformanceSpvUpload extends Model
{
    //
    protected $table = 'treshold_performance_new_spv_upload_result_absen';
    public $timestamps = true;
    protected $guarded=[];
}
