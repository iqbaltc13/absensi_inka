<?php

namespace App\Http\Middleware;

use Closure;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->username == 'admin_new_spv_absensi' && $request->password == 'secretapinewabsensi2020') {
            return $next($request);
        }
        else{
            return response()->json([
                'response_code' => 401,
                'message' => 'Username atau Password tidak valid',
                'errors' => NULL,
                'data' => NULL
            ]);
        }
        //return $next($request);
    }
}
