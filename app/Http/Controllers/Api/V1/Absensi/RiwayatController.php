<?php

namespace App\Http\Controllers\Api\V1\Absensi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use DateTime;
use App\User;
use App\Rfid;
use App\RfidTmp;
use App\RfidClean;
use stdClass;
use Log;
use App\MasterRfidReader;
use App\HT08KESHC;
use App\HT45KESHC;
use App\HT08MirroringCron;
use DateInterval;
use App\TresholdPerformanceNewSpv;
use App\MasterTresholdDateNewSpv;
use App\ExtractJadwalPegawai;
use App\Ht45RfidXExtractJadwal;
use App\TresholdPerformanceSpvUpload;

class RiwayatController extends Controller
{
    

    public function riwayat(Request $request){

        date_default_timezone_set("Asia/Bangkok");
        $now=new DateTime();
        $date=$request->date ? $request->date : $now->format('Y-m-d');

        $user=User::where('nip',$request->nip)->first();
        $arrSelect=[
            'nip',
            'created_at as waktu_deteksi',
            'rfid_tid as rfid_card',
            'rfid_arah as arah',
            'rfid_userdata as rfid_data',
            'rfid_reader_mac',
            'rfid_reader_ip',

        ];
        $parseQuery=[
            'nip'=>$request->nip,
            'user'=>$user,  
        ];
        $dataRfid=Rfid::with(['masterRfidReader'])->select($arrSelect);
        $dataRfid=$dataRfid->where(function($query) use ($parseQuery){
            $query->where('nip',$parseQuery['nip']);
            if($parseQuery['user']){
                $query->orWhere('rfid_tid',$parseQuery['user']->rfid_tid);
            }
        });
        $dataRfid=$dataRfid->whereDate('created_at',$date);
        $dataRfid=$dataRfid->get();
        $dataRfid->each(function ($item, $key) {
            $item->reader_name = $item->masterRfidReader ? $item->masterRfidReader->nama : $item->rfid_reader_mac;
            unset($item->masterRfidReader);
        });
        


        return response()->json($dataRfid);


    }
}
