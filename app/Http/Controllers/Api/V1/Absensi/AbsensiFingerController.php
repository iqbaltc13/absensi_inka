<?php

namespace App\Http\Controllers\Api\V1\Absensi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Rfid;
use DB;
use App\RfidClean;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Client;
use Auth;
use App\RfidTmp;
use stdClass;
use DateTime;
use DateInterval;
use App\MasterRfidReader;
use App\Console\Commands\TresholdingAbsensi;
use App\User;

class AbsensiFingerController extends Controller
{
    //
    public function __construct()
    {
        $this->supervisor=new TresholdingAbsensi();
        //$this->spv_absensi=new SupervisorAbsensiRfidWithS1();

        
    }
    public function injectKotorFromFingerNoTreshold(Request $request){
        if($request->token=="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjIwMDc3NjI0NDUsInN1YiI6OTkxNTAwMDE3LCJpc3MiOiJodHRwOi8vMTk4LjcxLjgwLjE4OToxNDA0NS9hcGkvdjEvYXV0ZW50aWthc2kvbG9naW4iLCJpYXQiOjE1MzQzNzY4NDUsIm5iZiI6MTUzNDM3Njg0NSwianRpIjoid1dVUVBvVUFNMXFxU2xKVyJ9.xOKT-uqo1Shn67N7WCvrpEz4DWHRry-h7lXP3BCDHc4"){
            $nip=$request->nip;
            $created_at=$request->created_at;
            $kodeinout=$request->kodeinout;
            $interval = new DateInterval('P1D');
            $dateTimeDetected= new DateTime($created_at);
            $curDateTimeDetected= new DateTime($created_at);
            $jam=$curDateTimeDetected->format('H');
            if($kodeinout==1 && $jam > 13){
                $curDateTimeDetected=$curDateTimeDetected->sub($interval);
                
            }
            $user=User::where('nip',$nip)->first();
            if($user){
                if($user->rfid_tid){
                    $kolom=$kodeinout==1 ? 'timeout' : 'timein' ;
                    $arrayCreate=[
                        'rfid_epc'=>NULL, 
                        'rfid_tid'=>$user->rfid_tid, 
                        'rfid_userdata'=>'00000000', 
                        'rfid_reader_ip'=>'192.168.22.152', 
                        'rfid_reader_mac'=>'6C-EC-A1-FE-A7-E5', 
                        'rfid_reader_ant'=>2, 
                        'created_at'=>$created_at,
                        'nip'=>$nip,
                        'flag_read_s1'=>1,
                    ];
                    
                    $createRfid=Rfid::create($arrayCreate);
                    $createTmp=RfidTmp::create($arrayCreate);
                    $ht45Lokal['id_tmp']=1;
                    $ht45Lokal['id_record']=1;
                    $ht45Lokal['nip']=$nip;
                    $ht45Lokal['kodeinout']=$kodeinout;
                    $ht45Lokal['datetimedetected']=$created_at;
                    $ht45Lokal['nomesin']=990;
                    $insertHt45Lokal=$this->supervisor->insertHt45LokalCustom($ht45Lokal);
                    $arrParse['id']=$insertHt45Lokal->id;
                    $arrReturnFromTrigger=$this->supervisor->triggerFingerHt45ToHt08($arrParse);
                    $tanggalClean=isset($arrReturnFromTrigger['tanggal_absen']) ? $arrReturnFromTrigger['tanggal_absen'] : $curDateTimeDetected->format('Y-m-d');
                    $tidClean=$user->rfid_tid;
                    $statusClean=$kodeinout == 1 ? 'pulang' : 'masuk';
                    $cekDataClean=RfidClean::where('status',$statusClean)
                    ->whereDate('tanggal',$tanggalClean)->where('rfid_tid',$tidClean)->first();
                    if(is_null($cekDataClean)){
                        $arrayCleanCreate=[
                            'rfid_epc'=>NULL, 
                            'rfid_tid'=>$tidClean, 
                            'rfid_userdata'=>'00000000', 
                            'rfid_reader_ip'=>'192.168.22.152', 
                            'rfid_reader_mac'=>'6C-EC-A1-FE-A7-E5', 
                            'rfid_reader_ant'=>2, 
                            'created_at'=>$created_at,
                            'nip'=>$nip,
                            'id_record'=>$createRfid->id,
                            'status'=>$statusClean,
                            'created_at_masuk'=> $kodeinout == 0 ? $created_at : NULL,
                            'created_at_pulang'=>$kodeinout ==1 ? $created_at : NULL,
                            'flag_ht08'=>1,
                            'flag_ht45'=>1,
                            'tanggal'=>$tanggalClean,
            
                        ];
                        $createRfidClean=RfidClean::create($arrayCleanCreate);
                    }
                    else{
                        if($kodeinout==1){
                            $dateTimeParse=new DateTime($created_at);
                            $dateTimeCur= new DateTime($cekDataClean->created_at_pulang);
                            if($dateTimeParse > $dateTimeCur){
                                $cekDataClean->created_at_pulang=$dateTimeParse->format('Y-m-d H:i:s');
                                $cekDataClean->save();
                            }
                        }
                        if($kodeinout==0){
                            $dateTimeParse=new DateTime($created_at);
                            $dateTimeCur= new DateTime($cekDataClean->created_at_masuk);
                            if($dateTimeParse < $dateTimeCur){
                                $cekDataClean->created_at_masuk=$dateTimeParse->format('Y-m-d H:i:s');
                                $cekDataClean->save();   
                            }
                        }

                    }

                    
                    $insert_ht45 = $this->supervisor->insertHt45RfidIgnoreError($createTmp->id, $createRfid->id, $nip, strval($kodeinout), $created_at);
                    error_reporting(0);
                    $result= new stdClass;
                    $result->rfid=$createRfid;
                    $result->rfid_tmp=$createTmp;
                    $result->rfid_clean=$cekDataClean ? $cekDataClean : $createRfidClean;
                    $result->ht45_keshc_lokal=$insertHt45Lokal;
                    $result->ht08_keshc_lokal=isset($arrReturnFromTrigger['data_ht08']) ? $arrReturnFromTrigger['data_ht08'] : NULL;
                    //$this->logs('Inject Rfid No Treshold By Api',json_encode($request->all()));
                    return $this->success($result);
                }
                else{
                    return $this->customResponse(401,'null rfid tid','RFID TID untuk '.$user->nip.' tidak ada',NULL);
                }
                
                
            }
               
            
            else{
                return $this->failure('user tidak ditemukan');
            }
        }
        else{
            return $this->failure('token tidak valid');
        }
        
    }




    public function injectKotorFromFingerNoTresholdParseFormParams($formParams){
        
        $nip=$formParams['nip'];
        $created_at=$formParams['created_at'];
        $kodeinout=$formParams['kodeinout'];
        $interval = new DateInterval('P1D');
        $dateTimeDetected= new DateTime($created_at);
        $curDateTimeDetected= new DateTime($created_at);
        $jam=$curDateTimeDetected->format('H');
        if($kodeinout==1 && $jam > 13){
            $curDateTimeDetected=$curDateTimeDetected->sub($interval);
            
        }
        $user=User::where('nip',$nip)->first();
        if($user){
            if($user->rfid_tid){
                $kolom=$kodeinout==1 ? 'timeout' : 'timein' ;
                $arrayCreate=[
                    'rfid_epc'=>NULL, 
                    'rfid_tid'=>$user->rfid_tid, 
                    'rfid_userdata'=>'00000000', 
                    'rfid_reader_ip'=>'192.168.22.152', 
                    'rfid_reader_mac'=>'6C-EC-A1-FE-A7-E5', 
                    'rfid_reader_ant'=>2, 
                    'created_at'=>$created_at,
                    'nip'=>$nip,
                    'flag_read_s1'=>1,
                ];
                
                $createRfid=Rfid::create($arrayCreate);
                $createTmp=RfidTmp::create($arrayCreate);
                $ht45Lokal['id_tmp']=1;
                $ht45Lokal['id_record']=1;
                $ht45Lokal['nip']=$nip;
                $ht45Lokal['kodeinout']=$kodeinout;
                $ht45Lokal['datetimedetected']=$created_at;
                $ht45Lokal['nomesin']=990;
                $insertHt45Lokal=$this->supervisor->insertHt45LokalCustom($ht45Lokal);
                $arrParse['id']=$insertHt45Lokal->id;
                $arrReturnFromTrigger=$this->supervisor->triggerFingerHt45ToHt08($arrParse);
                $tanggalClean=isset($arrReturnFromTrigger['tanggal_absen']) ? $arrReturnFromTrigger['tanggal_absen'] : $curDateTimeDetected->format('Y-m-d');
                $tidClean=$user->rfid_tid;
                $statusClean=$kodeinout == 1 ? 'pulang' : 'masuk';
                $cekDataClean=RfidClean::where('status',$statusClean)
                ->whereDate('tanggal',$tanggalClean)->where('rfid_tid',$tidClean)->first();
                if(is_null($cekDataClean)){
                    $arrayCleanCreate=[
                        'rfid_epc'=>NULL, 
                        'rfid_tid'=>$tidClean, 
                        'rfid_userdata'=>'00000000', 
                        'rfid_reader_ip'=>'192.168.22.152', 
                        'rfid_reader_mac'=>'6C-EC-A1-FE-A7-E5', 
                        'rfid_reader_ant'=>2, 
                        'created_at'=>$created_at,
                        'nip'=>$nip,
                        'id_record'=>$createRfid->id,
                        'status'=>$statusClean,
                        'created_at_masuk'=> $kodeinout == 0 ? $created_at : NULL,
                        'created_at_pulang'=>$kodeinout ==1 ? $created_at : NULL,
                        'flag_ht08'=>1,
                        'flag_ht45'=>1,
                        'tanggal'=>$tanggalClean,
        
                    ];
                    $createRfidClean=RfidClean::create($arrayCleanCreate);
                }
                else{
                    if($kodeinout==1){
                        $dateTimeParse=new DateTime($created_at);
                        $dateTimeCur= new DateTime($cekDataClean->created_at_pulang);
                        if($dateTimeParse > $dateTimeCur){
                            $cekDataClean->created_at_pulang=$dateTimeParse->format('Y-m-d H:i:s');
                            $cekDataClean->save();
                        }
                    }
                    if($kodeinout==0){
                        $dateTimeParse=new DateTime($created_at);
                        $dateTimeCur= new DateTime($cekDataClean->created_at_masuk);
                        if($dateTimeParse < $dateTimeCur){
                            $cekDataClean->created_at_masuk=$dateTimeParse->format('Y-m-d H:i:s');
                            $cekDataClean->save();   
                        }
                    }

                }

                
                $insert_ht45 = $this->supervisor->insertHt45RfidIgnoreError($createTmp->id, $createRfid->id, $nip, strval($kodeinout), $created_at);
                error_reporting(0);
                $result= new stdClass;
                $result->rfid=$createRfid;
                $result->rfid_tmp=$createTmp;
                $result->rfid_clean=$cekDataClean ? $cekDataClean : $createRfidClean;
                $result->ht45_keshc_lokal=$insertHt45Lokal;
                $result->ht08_keshc_lokal=isset($arrReturnFromTrigger['data_ht08']) ? $arrReturnFromTrigger['data_ht08'] : NULL;
                
                return $this->success($result);
            }
            else{
                return $this->customResponse(401,'null rfid tid','RFID TID untuk '.$user->nip.' tidak ada',NULL);
            }
            
            
        }
            
        
        else{
            return $this->failure('user tidak ditemukan');
        }
        
        
    }
}
