<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RangeBySchSetting extends Model
{
    //
    protected $table = 'rangebysch_settings';
    public $timestamps = true;
    protected $guarded=[];
}
