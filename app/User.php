<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nip',  'password', 'kode_otp_ganti_password', 'token_login_mobile', 'token_login_mobile_kadaluarsa', 'rfid_tid','versi_apk','total_poin_asli','total_poin_aligment'
    ];
    public function rfid()
    {
        return $this->hasMany('App\Rfid', 'rfid_tid', 'rfid_tid')
                ->select([
                    'rfid_tid' => 'rfid_card',
                    'rfid_userdata' => 'rfid_data',
                    'rfid_reader_mac' => 'reader_name',
                    'rfid_arah' => 'arah',
                    'created_at' => 'waktu_deteksi']);
    }
    public function getHt08(){
            return $this->hasMany('App\HT08MirroringCron','REGNO','nip');
        }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
}
