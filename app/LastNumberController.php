<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LastNumberController extends Model
{
    protected $table = 'last_number_controller';
    public $timestamps = true;
    protected $guarded=[];
}
