<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RfidClean extends Model
{
    //
    protected $table = 'ht45_rfid_clean';
    protected $primaryKey = 'id';
    protected $guarded=[];
    public function user()
	{
  
        return $this->belongsTo('App\User', 'rfid_tid', 'rfid_tid')->withTrashed();;
	}

    public function masterRfidReader(){
        return $this->belongsTo('App\MasterRfidReader', 'rfid_reader_mac', 'rfid_reader_mac');
    }
}
