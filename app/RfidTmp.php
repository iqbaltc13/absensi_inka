<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RfidTmp extends Model
{
    //
    protected $table = 'ht45_rfid_tmp';
    protected $primaryKey = 'id';
    // protected $fillable = ['rfid_epc', 'rfid_tid', 'rfid_userdata', 'rfid_reader_ip', 'rfid_reader_mac', 
    //                         'rfid_reader_ant', 'created_at','nip'];
    /*optimasi*/
    protected $guarded = [];
    public function rfid()
    {
        return $this->hasOne('App\Rfid',  ['rfid_tid', 'rfid_tid']);
    }

	public function user()
	{
  
        return $this->hasOne('App\User', 'rfid_tid', 'rfid_tid')->withTrashed();;
	}

    public function masterRfidReader(){
        return $this->belongsTo('App\MasterRfidReader', 'rfid_reader_mac', 'rfid_reader_mac');
    }
}
