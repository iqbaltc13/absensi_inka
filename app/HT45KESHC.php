<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HT45KESHC extends Model
{
    //
    protected $table = 'ht45_keshc';
    protected $guarded=[];
    
    public $timestamps = true;
}
