<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExtractJadwalPegawai extends Model
{
    protected $table = 'extract_jadwal_pegawai';
    public $timestamps = true;
    protected $guarded=[];
    public function user(){
		
		return $this->hasOne('App\User', 'nip','nip')->withTrashed();
	
    }
}
