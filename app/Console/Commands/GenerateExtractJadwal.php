<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DateInterval;
use DateTime;
use App\HT08MirroringCron;
use App\Ht01f;
use App\RangebyschHt04;
use App\ExtractJadwalPegawai;

class GenerateExtractJadwal extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:extract_jadwal {--start=} {--finish=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Extract Jadwal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->one_interval=new DateInterval('P1D');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $fromInterval= new DateInterval('P10D');
        $toInterval= new DateInterval('P8D');
        $startNow=new DateTime();
        $dateTimeStart= $startNow->sub($fromInterval);
        $endNow=new DateTime();
        $dateTimeEnd=$endNow->add($toInterval);
        
        $arrQuery['start_date']=$this->option('start') ? $this->option('start') : $dateTimeStart->format('Y-m-d');
        $arrQuery['end_date']=$this->option('finish') ? $this->option('finish') : $dateTimeEnd->format('Y-m-d');
        $this->generateEtractJadwal($arrQuery);
    }
    public function generateEtractJadwal($arrQuery=[]){
        $dataHt08=HT08MirroringCron::query();
        if(isset($arrQuery['start_date']) && isset($arrQuery['end_date'])){
            $dataHt08=$dataHt08->where(function($query) use ($arrQuery){
                $query->where('ABSDATE','<=',$arrQuery['end_date']);
                $query->where('ABSDATE','>=',$arrQuery['start_date']);
            });
        }
        // $dataHt08=$dataHt08->whereIn('REGNO',['991700068','661700153']);
        // $dataHt08=$dataHt08->whereDate('ABSDATE','2020-02-05');
        $dataHt08=$dataHt08->orderBy('ABSDATE','DESC');
        $dataHt08=$dataHt08->get();
        foreach ($dataHt08 as $key => $ht08) {
           
            $arrParseRangeMasuk=[
                'SCHTYPE'=>trim($ht08->SCHTYPE," "),
                'ABSDATE'=>$ht08->ABSDATE,
                'REGNO'=>$ht08->REGNO,

            ];
            echo "arrParse Range Masuk <br/>";
            print_r($arrParseRangeMasuk);
            $getResultRangeMasuk=$this->getRangeMasuk($arrParseRangeMasuk);
            echo "get Result Range Masuk <br/>";
            print_r($getResultRangeMasuk);
            $arrParseRangePulang=[
                'SCHTYPE'=>trim($ht08->SCHTYPE," "),
                'ABSDATE'=>$ht08->ABSDATE,
                'REGNO'=>$ht08->REGNO,

            ];
            echo "arrParse Range Pulang <br/>";
            print_r($arrParseRangePulang);
            $getResultRangePulang=$this->getRangePulang($arrParseRangePulang);
            echo "get Result Range Pulang <br/>";
            print_r($getResultRangePulang);
            $checkMasuk=[
                'tanggal_absen'=>$ht08->ABSDATE,
                'nip'=>$ht08->REGNO,
                'status'=>0,
            ];
            $updateMasuk=[
                'schtype'=>trim($ht08->SCHTYPE," "),
                'status_ht08'=>$ht08->STATUS,
                //'date_time_detected'=>$ht08->TIMEIN,
                'jenis_shift'=>$getResultRangeMasuk['jenis_shift'],
                'range_start'=>isset($getResultRangeMasuk['datetime_start']) ? $getResultRangeMasuk['datetime_start'] : NULL,
                'range_end' =>isset($getResultRangeMasuk['datetime_end']) ? $getResultRangeMasuk['datetime_end'] : NULL,
            ];
            $checkPulang=[
                'tanggal_absen'=>$ht08->ABSDATE,
                'nip'=>$ht08->REGNO,
                'status'=>1,
                

            ];
            $updatePulang=[
                'schtype'=>trim($ht08->SCHTYPE," "),
                'status_ht08'=>$ht08->STATUS,
                'jenis_shift'=>$getResultRangePulang['jenis_shift'],
                //'date_time_detected'=>$ht08->TIMEOUT,
                'range_start'=>isset($getResultRangePulang['datetime_start']) ? $getResultRangePulang['datetime_start'] : NULL,
                'range_end' =>isset($getResultRangePulang['datetime_end']) ? $getResultRangePulang['datetime_end'] : NULL,
            ];
            $generateJadwalMasuk=ExtractJadwalPegawai::updateOrCreate(
                $checkMasuk,$updateMasuk
            );
            $generateJadwalPulang=ExtractJadwalPegawai::updateOrCreate(
                $checkPulang,$updatePulang
            );
            
            

        }
        
        
    }

    public function getRangeMasuk($arrParse=[]){
        $returnRange=[];
        $returnRange['jenis_shift']=NULL;
        $rangeByschStart=RangebyschHt04::where('absen','1')->where('schtype',$arrParse['SCHTYPE']);
        $rangeByschEnd=RangebyschHt04::where('absen','1')->where('schtype',$arrParse['SCHTYPE']);
        
        if(str_contains($arrParse['SCHTYPE'], 'S1')){
            $arrParseHt01f['nip']=$arrParse['REGNO'];
            $arrParseHt01f['tanggal_absen']=$arrParse['ABSDATE'];
            $getJenisShift=$this->getJenisShift($arrParseHt01f);
            
            if($getJenisShift){
                $getJenisShift=trim($getJenisShift," ");
                $rangeByschStart=$rangeByschStart
                ->where('shiftcode',$getJenisShift);
                
                $rangeByschEnd=$rangeByschEnd
                ->where('shiftcode',$getJenisShift);
                
                $returnRange['jenis_shift']=$getJenisShift;

            }

        }
        $rangeByschStart=$rangeByschStart
        ->orderBy('kemaren','ASC')
        ->orderBy('range_start','ASC')
        ->first();
        echo "get Range Start Masuk <br/>";
        print_r($rangeByschStart);
        $rangeByschEnd=$rangeByschEnd
        ->orderBy('kemaren','DESC')
        ->orderBy('range_start','DESC')
        ->first();
        echo "get Range End Masuk <br/>";
        print_r($rangeByschEnd);
        if($rangeByschStart && $rangeByschEnd){
            $dateRangeBySchStart=new DateTime($arrParse['ABSDATE']);
            $dateRangeBySchEnd=new DateTime($arrParse['ABSDATE']);
            // if($rangeByschStart->kemaren=='1'){
            //     $dateRangeBySchStart=$dateRangeBySchStart->add($this->one_interval);
            // }
            if($rangeByschEnd->kemaren=='1'){
                $dateRangeBySchEnd=$dateRangeBySchEnd->add($this->one_interval);
            }
            $dateTimeRangeStart=new DateTime($dateRangeBySchStart->format('Y-m-d').' '.$rangeByschStart->range_start);
            $dateTimeRangeEnd=new DateTime($dateRangeBySchEnd->format('Y-m-d').' '.$rangeByschEnd->range_end);
            $returnRange['datetime_start']=$dateTimeRangeStart->format('Y-m-d H:i:s');
            $returnRange['datetime_end']=$dateTimeRangeEnd->format('Y-m-d H:i:s');
        }
        return $returnRange;
    }
    public function getRangePulang($arrParse=[]){
        $returnRange=[];
        $returnRange['jenis_shift']=NULL;
        $rangeByschStart=RangebyschHt04::where('absen','2')->where('schtype',$arrParse['SCHTYPE']);
        $rangeByschEnd=RangebyschHt04::where('absen','2')->where('schtype',$arrParse['SCHTYPE']);
        
        if(str_contains($arrParse['SCHTYPE'], 'S1')){
            $arrParseHt01f['nip']=$arrParse['REGNO'];
            $arrParseHt01f['tanggal_absen']=$arrParse['ABSDATE'];
            $getJenisShift=$this->getJenisShift($arrParseHt01f);
            if($getJenisShift){
                $getJenisShift=trim($getJenisShift," ");
                $rangeByschStart=$rangeByschStart
                ->where('shiftcode',$getJenisShift);
                
                $rangeByschEnd=$rangeByschEnd
                ->where('shiftcode',$getJenisShift);
                
                $returnRange['jenis_shift']=$getJenisShift;

            }

        }
        $rangeByschStart=$rangeByschStart
        ->orderBy('kemaren','ASC')
        ->orderBy('range_start','ASC')
        ->first();
        echo "get Range Start Pulang <br/>";
        print_r($rangeByschStart);
        $rangeByschEnd=$rangeByschEnd
        ->orderBy('kemaren','DESC')
        ->orderBy('range_start','DESC')
        ->first();
        echo "get Range  End Pulang <br/>";
        print_r($rangeByschEnd);
        if($rangeByschStart && $rangeByschEnd){
            $dateRangeBySchStart=new DateTime($arrParse['ABSDATE']);
            $dateRangeBySchEnd=new DateTime($arrParse['ABSDATE']);
            if($rangeByschStart->kemaren=='1'){
                $dateRangeBySchStart=$dateRangeBySchStart->add($this->one_interval);
            }
            if($rangeByschEnd->kemaren=='1'){
                $dateRangeBySchEnd=$dateRangeBySchEnd->add($this->one_interval);
            }
            $dateTimeRangeStart=new DateTime($dateRangeBySchStart->format('Y-m-d').' '.$rangeByschStart->range_start);
            $dateTimeRangeEnd=new DateTime($dateRangeBySchEnd->format('Y-m-d').' '.$rangeByschEnd->range_end);
            $returnRange['datetime_start']=$dateTimeRangeStart->format('Y-m-d H:i:s');
            $returnRange['datetime_end']=$dateTimeRangeEnd->format('Y-m-d H:i:s');
        }
        return $returnRange;
        
                
       
    }
    public function getJenisShift($arrParse=[]){
        
        $dateTime= new DateTime($arrParse['tanggal_absen']);
        $ht01f=Ht01f::where('PERIODE',$dateTime->format('Y'))
        ->where('BULAN',$dateTime->format('m'))
        ->where('REGNO',$arrParse['nip'])
        ->first();
        $tanggal=$dateTime->format('d')*1;
        $return = $ht01f ? $ht01f->{'DAY'.$tanggal} : NULL;
        return $return;

    }
}
