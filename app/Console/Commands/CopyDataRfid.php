<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\RfidTmp;
use DB;
use Carbon\Carbon;
use Log;
use DateTime;
use App\MasterRfidReader;
use App\HT08KESHC;
use App\HT45KESHC;
use DateInterval;
use App\Rfid;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;


class CopyDataRfid extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'copy:rfid';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to copy rfid';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client=new Client();
        $this->url= env('EOFFICE_PRD_URL', 'localhost').'api/v1/';
        
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $maxIdTmp=Rfid::query()->max('INDEKSFINGER');
        print_r($maxIdTmp);
        $this->copyDataFromRiwayatPrdToRiwayatDev($maxIdTmp);
        //
    }
    public function copyDataFromRiwayatPrdToRiwayatDev($maxId){
        //$dataTmp=RfidTmp::query()->where('id','>',$maxId)->orderBy('id','ASC')->take(100)->get();
        $take=100;
        $formParams=[
            
            'max_id' => $maxId,
            'take'   => $take,
        ];
        print_r($formParams);
        $url=$this->url.'get-riwayat-by-max-id';
        try{
            $getDataJson = $this->client->request('POST', $url, array('form_params' => $formParams));
        } catch (\Exception $e){
            return $maxId;
        }
        
        $getData =  json_decode($getDataJson->getBody()->getContents(), true);
        print_r($getData);
        
        $dataRiwayat =  $getData['data'];
        foreach ($dataRiwayat as $key => $value) {
            print_r($value);
            $arrCreate=[
                'rfid_tid'         =>$value['rfid_tid'],
                'rfid_reader_mac'  =>$value['rfid_reader_mac'],
                'created_at'       =>$value['created_at'],
            
            

                'INDEKSFINGER'     =>$value['id'],
                'guid'             =>$value['guid'],
                'rfid_epc'         =>$value['rfid_epc'],
                
                'rfid_userdata'    =>$value['rfid_userdata'],
                'rfid_arah'        =>$value['rfid_arah'],
                'rfid_reader_ip'   =>$value['rfid_reader_ip'],
                
                'rfid_reader_ant'  =>$value['rfid_reader_ant'],
                
                //'flag_read'        =>$value['flag_read'],
                'nip'              =>$value['nip'],
                'is_libur'         =>$value['is_libur'],
                'is_ht45'          =>$value['is_ht45'],
                'is_missing'       =>$value['is_missing'],
                
            ];
            $createTmp=RfidTmp::create($arrCreate);
            print_r($createTmp);
            $createRiwayatNewSpv=Rfid::create($arrCreate);
        }


        $dataMaxTmp=Rfid::query()->max('INDEKSFINGER');

        return $dataMaxTmp;
    }
}
