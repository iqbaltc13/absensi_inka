<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Psr7\Request as GuzzleRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ServerException;
use Carbon\Carbon;
use DB;
use DateTime;
use App\User;
use App\Rfid;
use App\RfidTmp;
use App\RfidClean;
use stdClass;
use App\MasterRfidReader;
use App\HT08KESHC;
use App\HT45KESHC;
use App\HT08MirroringCron;
use App\Console\Commands\TresholdingAbsensi;
use App\ExtractJadwalPegawai;
use App\Ht45RfidXExtractJadwal;
use App\TresholdPerformanceSpvUpload;

class UploadResultAbsensi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absensi:create-result {--kodeinout=} {--worker=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->supervisor= new TresholdingAbsensi();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $spv=$this->iterateCreateResult();
    }
    public function iterateCreateResult(){
        date_default_timezone_set("Asia/Bangkok");
        $dateTime=new DateTime();
        $dateProcess=Carbon::yesterday();
        $dateMulai = '2019-12-10';
        $status=$this->option('kodeinout');
        $worker=$this->option('worker');
        $dataExtractJadwalXHt45Rfid=Ht45RfidXExtractJadwal::
        whereNull('flag_upload');
        if(isset($worker)){
            //jangan lupa ubah command di upload
            $masterRfidReader=MasterRfidReader::where('worker',$worker)->get();
            //$dataExtractJadwalXHt45Rfid=$dataExtractJadwalXHt45Rfid->where('status_pulang_masuk',$status);
            $dataExtractJadwalXHt45Rfid=$dataExtractJadwalXHt45Rfid->whereIn('rfid_reader_mac',$masterRfidReader
            ->pluck('rfid_reader_mac'));
        }
        $dataExtractJadwalXHt45Rfid=$dataExtractJadwalXHt45Rfid->take(100)
        ->get();
        print_r(sizeof($dataExtractJadwalXHt45Rfid));
        $start= new DateTime();
        
        $jumlahData=sizeof($dataExtractJadwalXHt45Rfid);
        $dataExtractJadwalXHt45Rfid->each(function ($item, $key){
            $jsonUpload=$this->supervisor->tresholdingHt45RfidXExtractJadwal($item);
        
        });
        $finish= new DateTime();
        $arrInsertPerformance=[
            'start'=>$start->format('Y-m-d H:i:s'),
            'finish'=>$finish->format('Y-m-d H:i:s'),
            'worker'=>$worker,
            'data'=>$jumlahData
        ];
        if($jumlahData > 0){
            $jsonUpload=$this->supervisor->createTresholdPerformanceSpvUpload($arrInsertPerformance);
        }
    }
    public function updateRfidKotor($createdAt,$rfidTid,$nip=NULL,$isLibur=NULL,$isHt45=NULL,$flagReadS1=NULL){
        $arrUpdateRfid=[
            'flag_read_s1' => $flagReadS1,
            'is_libur'  => $isLibur,
            'is_ht45'  => $isHt45,
            'nip' => $nip,
        ];
        $updateRfid=Rfid::where('created_at',$createdAt)
        ->where('rfid_tid',$rfidTid)
        ->update($arrUpdateRfid);
        $dataRfid=Rfid::where('created_at',$createdAt)
        ->where('rfid_tid',$rfidTid)
        ->first();

        return $dataRfid; 
    }
    public function doneUpload($id){
        $arrUpdate=[
            'flag_upload_keshc'=>1
        ];
        HT45KESHC::where('id',$id)->update($arrUpdate);
    }

    public function uploadKeshc( $arrParse=[],$nip, $kodeInOut, $dateTimeDetected){
       
        
        
            $url = env('API_KESHC_NEW_URL', 'localhost').'api/inka/ht45-insert';
            $client = new Client();
            $formParamsToday = [
                'username' => env('API_KESHC_USERNAME'),
                'password' => env('API_KESHC_PASSWORD'),
                'nip' => $nip,
                'kodeinout' => $kodeInOut,
                'tglload' => $dateTimeDetected
            ];
            $result = false;
            try{
                $data = $client->request('POST', $url, array('form_params' => $formParamsToday));
                $resultInsert =  json_decode($data->getBody()->getContents());
                // RfidClean::where('id_record', $id_record)->update(['flag_ht45' => '1']);
                Log::info('>>> sukses insert_ht45_device_rfid <<<');
                Log::info(json_encode($resultInsert));
                HT45KESHC::where('id',$arrParse['id'])->update(['flag_upload_keshc' => 1]);
                return true;
            } catch (\Exception $e){
                HT45KESHC::where('id',$arrParse['id'])->update(['flag_upload_keshc' => 2]);
                //echo $e->getResponse()->getBody();
                //die();
                // $this->errorIndexTimeStamp  = 2;
                // echo "WARNING: gagal memasukkan tid: ".$id_record." || tglload : ".$dateTimeDetected." || now : ".date("Y-m-d H:i:s", strtotime("now"))." \n";
                // echo $e->getMessage();
                $strDuplicate = "Cannot insert duplicate key row in object 'dbo.HT45' with unique index 'regnotglloadidx'.";
                if (strpos($e->getResponse()->getBody(), $strDuplicate) !== false) {
                    // duplicate handling
                    // Rfid::where('id', $id_record)->update(['flag_read_s1' => '2']);
                    // RfidTmp::where('id', $id_tmp)->update(['flag_read_s1' => '2']);
                    // RfidClean::where('id_record', $id_record)->update(['flag_ht45' => '2']);
                } else {
                    // RfidTmp::where('id', $id_tmp)->update(['flag_read_s1' => '0']);
                    // echo "WARNING: gagal memasukkan tid: ".$id_tmp. "dengan pesan error berikut: ".$e->getResponse()->getBody();
                }
                // Rfid::where('id', $id_record)->update(['is_ht45' => '0']);
            }

            if(isset($resultInsert->response_code)) :
                // RfidTmp::where('id', $id_record)->update(['flag_read_s1' => '0']);
                // echo "WARNING: URL : ".$url."|| now : ".date("Y-m-d H:i:s", strtotime("now"))." \n";
                // print_r($resultInsert);
            endif;
        
    }
    public function doneUploadHt08($id,$status){
        RfidClean::where('id',$id)->update(['flag_ht08'=>$status]);
    }

    public function tresholdingHt45RfidXExtractJadwal(){
        
    }
}
