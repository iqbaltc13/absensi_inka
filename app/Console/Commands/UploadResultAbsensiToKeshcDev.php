<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\RfidClean;
use App\Helpers\Helper;
use DateTime; 
use App\LastNumberController;
use Illuminate\Support\Collection;
use App\Http\Controllers\Api\V1\Absensi\AbsensiFingerController;

class UploadResultAbsensiToKeshcDev extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absensi:upload-result-dev';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Absensi upload result to keshc dev';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->helper               = new Helper();
        $this->absensi_controller   = new AbsensiFingerController();
        $this->take_data_finger     = 50;
        $this->nomesin_finger       = 990;
        $this->nama_data_max_table  = 'data absen finger masuk dan pulang';
        
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $helper= new Helper();

        
        $dataCleanMasuk=RfidClean::where('status','masuk')
        ->where('tanggal','>=','2020-02-29')
        
        //->where('id','>',16861)
        ->where('flag_ht08','!=','7')
        ->take(100)
        //->where('id',17669)
        ->get();
        $dataCleanPulang=RfidClean::where('status','pulang')
        ->where('tanggal','>=','2020-02-29')
        //->where('id','>',15434)
        ->where('flag_ht08','!=','7')
        ->take(100)
        //->where('id',17006)
        ->get();

        $dataCleanMasuk->each(function($item,$key) use ($helper){
            $dateTimeClean=new DateTime($item->tanggal);
            $arrParse['tanggal']=$dateTimeClean->format('Y-m-d');
            $arrParse['nip']=$item->nip;
            $arrParse['kodeinout']=0;
            $arrParse['date_time_detected']=$item->created_at_masuk;
            print_r($arrParse);
            $helper->injectHt45Ht08KeshcDev($arrParse);
            $item->flag_ht08='7';
            $item->save();
            $arrParse=[];
            
        });
        $dataCleanPulang->each(function($item,$key) use ($helper){
            $dateTimeClean=new DateTime($item->tanggal);
            $arrParse['tanggal']=$dateTimeClean->format('Y-m-d');
            $arrParse['nip']=$item->nip;
            $arrParse['kodeinout']=1;
            $arrParse['date_time_detected']=$item->created_at_pulang;
            print_r($arrParse);
            $helper->injectHt45Ht08KeshcDev($arrParse);
            $item->flag_ht08='7';
            $item->save();
            $arrParse=[];
        });
        $this->prosesDataFinger();
    }

    public function prosesDataFinger(){
        $maxId      = $this->getMaxIdDataFinger();
        $nomesin    = $this->nomesin_finger;
        $take       = $this->take_data_finger;
        $formParams=[
            'id'       => $maxId,
            'nomesin'  => $nomesin,
            'take'     => $take,
        ];
        print_r($formParams);
        $data= $this->helper->getDataFinger($formParams);
        if(sizeof($data) > 0){
            $collectFinger =  collect($data['data']);
            $maxId         =  $collectFinger->max('id');
            foreach ($collectFinger as $key => $value) {
                $formParamsProsesFinger=[
                    'nip'        => $value['REGNO'],
                    'kodeinout'  => $value['KODEINOUT'],
                    'created_at' => $value['TGLLOAD'],
                    'reader_mac' => 'C',
                ];
                print_r($formParamsProsesFinger);
                $this->absensi_controller->injectKotorFromFingerNoTresholdParseFormParams($formParamsProsesFinger);
            }
            
        }
        $updateLastNumber = $this->setMaxIdDataFinger($maxId);
        //dd($updateLastNumber);
    }
    public function getMaxIdDataFinger(){
        $data=LastNumberController::where('nama',$this->nama_data_max_table)->first();
        $return = $data ? $data->last_number : 0 ;
        return $return;
    }

    public function setMaxIdDataFinger($id){
        $update=NULL;
        if(!is_null($id) && $id !=0){
            $update=LastNumberController::where('nama',$this->nama_data_max_table)->update([
                'last_number' => $id,
            ]);
        }
        

        return $update;
    }
}
