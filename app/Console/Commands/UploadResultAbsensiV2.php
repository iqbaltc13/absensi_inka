<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\RfidClean;
use App\Helpers\Helper;
use DateTime; 

class UploadResultAbsensiV2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absensi:upload-result';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Absensi upload result';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->helper= new Helper();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $helper= new Helper();
        $dataCleanMasuk=RfidClean::where('status','masuk')
        ->where('tanggal','>=','2020-02-13')
        
        //->where('id','>',16861)
        ->whereNull('flag_ht45')
        ->take(100)
        //->where('id',17669)
        ->get();
        $dataCleanPulang=RfidClean::where('status','pulang')
        ->where('tanggal','>=','2020-02-12')
        //->where('id','>',15434)
        ->whereNull('flag_ht45')
        ->take(100)
        //->where('id',17006)
        ->get();

        $dataCleanMasuk->each(function($item,$key) use ($helper){
            $dateTimeClean=new DateTime($item->tanggal);
            $arrParse['tanggal']=$dateTimeClean->format('Y-m-d');
            $arrParse['nip']=$item->nip;
            $arrParse['kodeinout']=0;
            $arrParse['date_time_detected']=$item->created_at_masuk;
            print_r($arrParse);
            $helper->injectHt45Ht08KeshcPrd($arrParse);
            $item->flag_ht45=1;
            $item->save();
            $arrParse=[];
            
        });
        $dataCleanPulang->each(function($item,$key) use ($helper){
            $dateTimeClean=new DateTime($item->tanggal);
            $arrParse['tanggal']=$dateTimeClean->format('Y-m-d');
            $arrParse['nip']=$item->nip;
            $arrParse['kodeinout']=1;
            $arrParse['date_time_detected']=$item->created_at_pulang;
            print_r($arrParse);
            $helper->injectHt45Ht08KeshcPrd($arrParse);
            $item->flag_ht45=1;
            $item->save();
            $arrParse=[];
        });
    }
    

    
}
